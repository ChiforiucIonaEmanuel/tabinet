#include <iostream>
#include "Card.h"
#include "Deck.h"
#include "Player.h"
#include "Game.h"

int main()
{
	Game game;

	game.GameInit();
	game.Run();
	return 0;
}