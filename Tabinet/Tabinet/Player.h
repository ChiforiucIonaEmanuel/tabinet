#pragma once
#include "Card.h"
#include<string>
#include <vector>


class Player
{
private:
	std::string m_name;
	int m_cards_number;
	int m_score;
	std::vector <Card> m_hand;
	std::vector <Card> m_board; //cartile pe care le ia de jos
	int m_team;
public:
	Player();
	~Player();

	void PrintHand() const;

	void PrintBoard() const;

	void PushCard(Card card);

	void PopCard();

	void EraseCardPlayer(int index);

	Card TopHand();

	Card GetCard(int index);

	std::vector<Card> GetHand();

	std::vector<Card> GetBoard();

	int GetScore();

	void AddOnePoint();

	void AddScore(int x);

	void CalculateScore();

	bool HandIsEmpty();

	void AddToBoard(Card card);
	
	void SetName(std::string x);

	std::string GetName()const;

	void SetTeam(int x);

	int GetTeam();
	
	int GetNrOfCards()const ;

	int GetIndexFromHand(Card c)const;
	
	friend std::ostream& operator << (std::ostream& os, const Player& player);
};

