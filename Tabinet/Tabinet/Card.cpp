#include "Card.h"
#include <cassert>
#include <string>
Card::Card() :
	Card(0,0)
{

}

Card::Card(uint16_t symbol, uint16_t number) 
{
	m_symbol = symbol;
	m_number = number;
}

Card::~Card()
{
	m_symbol = 0;
	m_number = 0;
}

std::string Card::GetNumber() const
{
    
	if (m_number == 11) return "A";
    
	if (m_number >= 2 && m_number<=10) return std::to_string(m_number);
    
    if (m_number == 12) return "J";
    if (m_number == 13) return "Q";
	if (m_number == 14) return "K";
	else return "Unknown number!";
}

int Card::GetValue()
{
	return m_number;
}

const Card& Card::operator=(const Card& c)
{
	this->m_number = c.m_number;
	this->m_symbol = c.m_symbol;
	return *this;
}

std::string Card::GetSymbol() const
{
	
		if (m_symbol == 1) return "Diamonds";
		if (m_symbol == 2) return "Hearts";
		if (m_symbol == 3) return "Spades";
		if (m_symbol == 4) return "Clubs";
		else return "Unknown symbol!";
}

std::ostream& operator<<(std::ostream& os, const Card& card)
{
	os << card.GetNumber() << " of " << card.GetSymbol();
	return os;
}

bool operator==(Card card1, Card card2)
{
	if (card1.GetValue() == card2.GetValue() &&
		card1.GetSymbol() == card2.GetSymbol())
	{
		return true;
	}
	return false;
}

