#include "Player.h"
#include "Deck.h"
#include "Card.h"

Player::Player()
{
	m_name = "Unknown";
	m_cards_number = 0;
	m_score = 0;
	std::vector<Card>m_hand(m_cards_number, Card());
	std::vector<Card>m_board(52, Card());
	m_team = 0;
}

Player::~Player()
{
}

void Player::PrintHand() const
{
	for (int index = 0; index < m_hand.size(); index++)
	{
		std::cout << index << ") " << m_hand.at(index) << std::endl;
	}
	std::cout << std::endl;
}

void Player::PrintBoard() const
{
	std::cout << "Board: ";
	for (int index = 0; index < m_board.size(); index++)
	{
		std::cout << m_board.at(index) << ", ";
	}
	std::cout << std::endl;
}

void Player::PushCard(Card card)
{
	m_hand.push_back(card);
	m_cards_number++;
}

void Player::PopCard()
{
	m_hand.pop_back();
	m_cards_number--;
}

void Player::EraseCardPlayer(int index)
{
	m_hand.erase(m_hand.begin() + index);
	m_cards_number--;
}

Card Player::TopHand()
{
	return m_hand.at(m_hand.size() - 1);
}

Card Player::GetCard(int index)
{
	return m_hand.at(index);
}

std::vector<Card> Player::GetHand()
{
	return this->m_hand;
}

std::vector<Card> Player::GetBoard()
{
	return this->m_board;
}

int Player::GetScore()
{
	return this->m_score;
}

void Player::AddOnePoint()
{
	this->m_score++;
}

void Player::AddScore(int x)
{
	this->m_score+=x;
}

void Player::CalculateScore()
{
	int pointValue = 0;
	for (int i = 0; i < this->m_board.size(); i++)
	{
		int pointValue = 0;
		if (this->m_board.at(i).GetValue() >= 10 && this->m_board.at(i).GetSymbol() != "Diamonds") {
			pointValue = 1;
 		}
		if (this->m_board.at(i).GetValue() == 2 && this->m_board.at(i).GetSymbol() == "Clubs") {
			pointValue = 2; 
		}
		if (this->m_board.at(i).GetValue() == 10 && this->m_board.at(i).GetSymbol() == "Diamonds") {
			pointValue = 2; 
		}
		this->AddScore(pointValue);
		
	}
	
	GetScore();
}

bool Player::HandIsEmpty()
{
	if (this->m_cards_number == 0)
	{
		return true;
	}

	return false;
}

void Player::AddToBoard(Card card)
{

	this->m_board.push_back(card);

}

void Player::SetName(std::string x)
{
	m_name = x;
}

std::string Player::GetName() const
{
	return Player::m_name;
}

void Player::SetTeam(int x)
{
	m_team = x;
}

int Player::GetTeam()
{
	return m_team;
}

int Player::GetNrOfCards()const
{
	return m_cards_number;
}

int Player::GetIndexFromHand(Card c)const
{
	for (int i = 0; i < m_hand.size(); i++)
	{
		if (c.GetNumber() == m_hand.at(i).GetNumber() &&
			c.GetSymbol() == m_hand.at(i).GetSymbol())
			return i;
	}
}

std::ostream& operator<<(std::ostream& os, const Player& player)
{
	player.PrintBoard();
	
	os << player.GetName() << " have " << player.GetNrOfCards() << " cards:" << std::endl;

	player.PrintHand();

	return os;
}
