#include "Deck.h"
#include "Player.h"
Deck::Deck()
{
	std::vector<Card> m_deck(MAXSIZE, Card());
}

Deck::~Deck()
{
}

void Deck::CreateDeck()
{
	int length = 0;
	Card afisarePeColoane[13][4];

	
	Card card;
	for (int indexN = 2; indexN <= 14; indexN++)
	{
		for (int indexS = 1; indexS <= 4; indexS++)
		{
			card = Card(indexS, indexN);
			m_deck.push_back(card);
		}
	}
	
	std::cout << "Initial deck:" << std::endl;
	int space = 1;
	for (int index = 0; index < m_deck.size(); index++)
	{
		if (space % 4 != 0)
		{
			std::cout << m_deck.at(index) << " | ";
			space++;
		}
		else
		{
			std::cout << m_deck.at(index) << std::endl;
			space++;
		}
	}
}

void Deck::Shuffle()
{
	srand(time(NULL));
	for (int index1 = m_deck.size() - 1; index1 > 0; index1--)
	{
		int index2 = (rand() % (index1 + 1));
		std::swap(m_deck.at(index2), m_deck.at(index1));
	}
}

void Deck::PrintDeck()
{
	for (int index = 0; index < m_deck.size(); index++)
	{
		std::cout <<index+1<<") "<< m_deck.at(index) << std::endl;
	}
	
}

bool Deck::IsFull()
{
	if (m_deck.size() >= MAXSIZE)
	{
		return true;
	}
	return false;
}

bool Deck::IsEmpty()
{
		if (m_deck.size() == 0)
		{
			return true;
		}

		return false;
}

void Deck::PopDeck()
{
	if (IsEmpty() == false)
	{
		m_deck.pop_back();
	}
}

Card Deck::TopDeck()
{
	if (IsEmpty() == false)
	{
		return m_deck.at(m_deck.size() - 1);
	}
	return Card(0,0);
	
}

void Deck::PushBackCard(Card card)
{
	m_deck.push_back(card);
}

Card Deck::getCard()
{
	return Card();
}

int Deck::GetCardsLeft()
{
	return m_deck.size();
}

