#include "Game.h"
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include<algorithm>
Game::Game()
{
	m_nr_of_players = 0;
}

Game::~Game()
{
}

void Game::InitPlayers()
{

	std::cout << "Enter the number of players:";
	std::cin >> m_nr_of_players;
	system("cls");
	PrintBanner();
	int turn = 0;
	int teammate;

	if (ValidatePlayers())
	{
		if (m_nr_of_players == 4)
		{
			this->m_player_list = std::vector<Player>(m_nr_of_players, Player());
			std::string name;

			this->m_deck.CreateDeck();
			std::cout << std::endl;
			this->m_deck.Shuffle();


			for (int i = 0; i < m_nr_of_players; i++)
			{
				std::cout << "Enter the player's name:";
				std::cin >> name;
				this->m_player_list.at(i).SetName(name);

			}

			std::cout << "Create teams!\n Team 1:\n";
			std::cin >> teammate;
			this->m_player_list.at(teammate).SetTeam(1);
			std::cin >> teammate;
			this->m_player_list.at(teammate).SetTeam(1);
			std::cout << "\n Team 2:\n";
			std::cin >> teammate;
			this->m_player_list.at(teammate).SetTeam(2);
			std::cin >> teammate;
			this->m_player_list.at(teammate).SetTeam(2);

			for (int nr_carti = 0; nr_carti < 4 * m_nr_of_players; nr_carti++)
			{
				this->m_player_list.at(turn % m_nr_of_players).PushCard(this->m_deck.TopDeck());
				this->m_deck.PopDeck();
				turn++;
			}

			LoadingInit();
			for (int i = 0; i < m_nr_of_players; i++)
			{
				std::cout << this->m_player_list.at(i);
			}
		}
		if (m_nr_of_players == 2 || m_nr_of_players == 3)
		{
			this->m_player_list = std::vector<Player>(m_nr_of_players, Player());
			std::string name;

			this->m_deck.CreateDeck();
			std::cout << std::endl;
			this->m_deck.Shuffle();


			for (int i = 0; i < m_nr_of_players; i++)
			{
				std::cout << "Enter the player's name:";
				std::cin >> name;
				this->m_player_list.at(i).SetName(name);

			}

			for (int nr_carti = 0; nr_carti < 4 * m_nr_of_players; nr_carti++)
			{
				this->m_player_list.at(turn % m_nr_of_players).PushCard(this->m_deck.TopDeck());
				this->m_deck.PopDeck();
				turn++;
			}

			LoadingInit();
			for (int i = 0; i < m_nr_of_players; i++)
			{
				std::cout << this->m_player_list.at(i);
			}
		}
	}
	else {
		std::cout << "Invalid number of players!" << std::endl
			<< "The game starts with 2-4 players.";
		exit(0);

	}
}




void Game::GameInit()
{
	PrintBanner();
	InitPlayers();
	ReturnCards();
	for (int index = 0; index < 4; index++)
	{
		this->m_table.push_back(this->m_deck.TopDeck());
		this->m_deck.PopDeck();
	}
	std::cout << "The cards on the table are: \n";
	for (int index = 0; index < m_table.size(); index++)
	{
		std::cout << index << ") " << m_table.at(index) << std::endl;
	}
	std::cout << std::endl;
	Sleep(3000);
}

void Game::Run()
{
	bool running = true;
	int turn = 0;
	int answer;
	int playerCard, tableCard1, tableCard2;
	while (running)
	{
		while (!this->m_player_list.at(turn % m_nr_of_players).HandIsEmpty())

		{
			std::cout << this->m_player_list.at(turn % m_nr_of_players).GetName() << ", do you want to drop a card on table (1) or do you want to draw cards from the table (2)?\n 0)End turn.\n";
			std::cin >> answer;
			switch (answer)
			{
			case 2:
				std::cout << this->m_player_list.at(turn % m_nr_of_players).GetName() << ", which card do you want to use?\n"; std::cin >> playerCard;
				std::cout << this->m_player_list.at(turn % m_nr_of_players).GetName() << ", which cards do you want to pull from table?\n"; std::cin >> tableCard1 >> tableCard2;
				TakePoints(turn % m_nr_of_players, playerCard, tableCard1, tableCard2);

				system("cls");
				PrintBanner();
				for (int i = 0; i < m_nr_of_players; i++)
				{
					std::cout << this->m_player_list.at(i);

				}
				std::cout << "The cards on the table are: \n";
				for (int index = 0; index < m_table.size(); index++)
				{
					std::cout << index << ") " << m_table.at(index) << std::endl;
				}
				std::cout << std::endl;
				//Sleep(3000);
				if (this->m_table.size() == 0)
				{
					this->m_player_list.at(turn % m_nr_of_players).AddOnePoint();
				}
				break;
			case 1:
				std::cout << "Which card do you want to drop?\n"; std::cin >> playerCard;
				DropCard(turn % m_nr_of_players, playerCard);

				system("cls");
				PrintBanner();
				for (int i = 0; i < m_nr_of_players; i++)
				{

					std::cout << this->m_player_list.at(i);
				}
				std::cout << "The cards on the table are: \n";
				for (int index = 0; index < m_table.size(); index++)
				{
					std::cout << index << ") " << m_table.at(index) << std::endl;
				}
				std::cout << std::endl;
				//Sleep(3000);

				break;
			case 0:
				break;
			default:
				break;
			}
			turn++;
			system("cls");
			PrintBanner();
			for (int i = 0; i < m_nr_of_players; i++)
			{
				this->m_player_list.at(i).GetScore(); std::cout << "\n";
				std::cout << this->m_player_list.at(i);
			}
			std::cout << "The cards on the table are: \n";
			for (int index = 0; index < m_table.size(); index++)
			{
				std::cout << index << ") " << m_table.at(index) << std::endl;
			}
			std::cout << std::endl;
			//Sleep(3000);
		}

		if (this->m_player_list.back().HandIsEmpty())
		{
			if (this->m_deck.IsEmpty())
			{
				running = false;
			}
			else
			{
				system("cls");
				PrintBanner();


				for (int nr_carti = 0; nr_carti < 6 * m_nr_of_players && !this->m_deck.IsEmpty(); nr_carti++)
				{
					this->m_player_list.at(turn % m_nr_of_players).PushCard(this->m_deck.TopDeck());
					this->m_deck.PopDeck();
					turn++;
				}


				for (int i = 0; i < m_nr_of_players; i++)
				{
					std::cout << this->m_player_list.at(i);
				}
				std::cout << "The cards on the table are: \n";
				for (int index = 0; index < m_table.size(); index++)
				{
					std::cout << index << ") " << m_table.at(index) << std::endl;
				}
				std::cout << std::endl;
			}
		}
		std::cout << this->m_deck.GetCardsLeft() << "cards left in the deck.\n\n";
	}
	system("cls");
	PrintBanner();

	std::cout << "========================\n";
	std::cout << "=      END OF GAME     =\n";
	std::cout << "========================\n\n\n";
	for (Player p : this->m_player_list)
	{
		p.CalculateScore();
		std::cout << p.GetName() << " have " << p.GetScore() << " points!\n";
	}

	if (m_nr_of_players == 2)
	{
		if (this->m_player_list.at(0).GetScore() > this->m_player_list.at(1).GetScore())
			std::cout << this->m_player_list.at(0).GetName() << " wins!\n";
		else std::cout << this->m_player_list.at(1).GetName() << " wins!\n";
	}

	if (m_nr_of_players == 3)
	{
		if ((this->m_player_list.at(0).GetScore() > this->m_player_list.at(1).GetScore()) &&
			(this->m_player_list.at(0).GetScore() > this->m_player_list.at(2).GetScore()))
			std::cout << this->m_player_list.at(0).GetName() << " wins!\n";
		if ((this->m_player_list.at(1).GetScore() > this->m_player_list.at(0).GetScore()) &&
			(this->m_player_list.at(1).GetScore() > this->m_player_list.at(2).GetScore()))
			std::cout << this->m_player_list.at(1).GetName() << " wins!\n";
		if ((this->m_player_list.at(2).GetScore() > this->m_player_list.at(0).GetScore()) &&
			(this->m_player_list.at(2).GetScore() > this->m_player_list.at(1).GetScore()))
			std::cout << this->m_player_list.at(0).GetName() << " wins!\n";
	}

	if (m_nr_of_players == 4)
	{
		int t1score = 0, t2score = 0;
		for (Player p : m_player_list)
		{
			if (p.GetTeam() == 1)
			{
				t1score += p.GetScore();
			}
			else t2score += p.GetScore();
		}
		if (t1score > t1score)
		{
			std::cout << "TEAM 1 WINS!\n";
		}
		else
		{
			std::cout << "TEAM 2 WINS\n";
		}
	}
	//else Winner();
	//Winner();
}

void Game::PrintBanner() const
{
	std::cout << "==================\n";
	std::cout << "=     TABINET    =\n";
	std::cout << "==================\n";
}

void Game::LoadingInit()
{
	system("cls");
	PrintBanner();
	std::cout << "Playing cards are dealt!\nPlease wait.\n";

	for (int a = 0; a < 5; a++)
	{
		std::cout << "* ";
		Sleep(500);
	}

	system("cls");
	PrintBanner();
}

void Game::SetNrOfPlayers(int nr)
{
	m_nr_of_players = nr;
}

int Game::GetNrOfPlayers()
{
	return m_nr_of_players;
}

bool Game::NoPlayerCardsLeft()
{
	for (Player at : this->m_player_list)
	{
		if (!at.HandIsEmpty())
			return false;
	}
	return true;
}

void Game::EraseCardsTable(int index1, int index2)
{
	Card card1, card2;
	card1 = this->m_table.at(index1);
	card2 = this->m_table.at(index2);

	for (int i = 0; i < this->m_table.size(); i++)
	{
		if (m_table.at(i) == card1)
		{
			m_table.erase(m_table.begin() + i);
		}
	}
	for (int i = 0; i < this->m_table.size(); i++)
	{
		if (m_table.at(i) == card2)
		{
			m_table.erase(m_table.begin() + i);
		}
	}

}

bool Game::ValidatePlayers()const
{
	if (m_nr_of_players < 2 || m_nr_of_players > 4)
	{
		return false;
	}

	else return true;
}

void Game::ReturnCards()
{

	std::cout << this->m_player_list.at(0).GetName() << ", do you want to keep current cards? [D/N]" << std::endl;
	char raspuns;
	std::cin >> raspuns;

	switch (std::toupper(raspuns))
	{
	case 'D':
		LoadingInit();
		for (int i = 0; i < m_nr_of_players; i++)
		{
			for (int nr_carti = 0; nr_carti < 2; nr_carti++)
			{
				this->m_player_list.at(i % m_nr_of_players).PushCard(this->m_deck.TopDeck());
				this->m_deck.PopDeck();
			}
		}

		system("cls");
		PrintBanner();
		break;
	case 'N':
		LoadingInit();
		while (!this->m_player_list.at(0).HandIsEmpty())
		{
			this->m_deck.PushBackCard(this->m_player_list.at(0).TopHand());
			this->m_player_list.at(0).PopCard();
		}
		this->m_deck.Shuffle();
		for (int index = 0; index < 4; index++)
		{
			this->m_player_list.at(0).PushCard(this->m_deck.TopDeck());
			this->m_deck.PopDeck();
		}

		for (int i = 0; i < m_nr_of_players; i++)
		{
			for (int nr_carti = 0; nr_carti < 2; nr_carti++)
			{
				this->m_player_list.at(i).PushCard(this->m_deck.TopDeck());
				this->m_deck.PopDeck();
			}
		}

		system("cls");
		PrintBanner();
		break;
	default:
		break;
	}
	std::cout << "==================" << std::endl;
	std::cout << "THE NEW CARDS ARE:" << std::endl;
	std::cout << "==================\n" << std::endl;
	for (int i = 0; i < m_nr_of_players; i++)
	{
		std::cout << m_player_list.at(i);
	}

}

Deck Game::GetDeck()
{
	return m_deck;
}

void Game::Winner()
{
	int winner = 0;
	int maxScore = 0;
	for (int index = 0; index < this->m_player_list.size(); index++)
	{
		if (this->m_player_list.at(index).GetScore() >= maxScore)
		{
			maxScore = this->m_player_list.at(index).GetScore();
			winner = index;
		}
	}
	std::cout << "==================================\n";
	std::cout << "The winner is: " << this->m_player_list.at(winner).GetName() << "!!\n";
	std::cout << "==================================\n";

}

int Game::GetIndexFromTable(Card c) const
{
	for (int i = 0; i < m_table.size(); i++)
	{
		if (c.GetNumber() == m_table.at(i).GetNumber() &&
			c.GetSymbol() == m_table.at(i).GetSymbol())
			return i;
	}
}



void Game::TakePoints(int index, int playerCard, int tableCard1, int tableCard2)
{
	char answer = 'D';
	while (std::toupper(answer) == 'D')
	{


		if (this->m_player_list.at(index).GetCard(playerCard).GetValue() == this->m_table.at(tableCard1).GetValue() + this->m_table.at(tableCard2).GetValue())
		{
			this->m_player_list.at(index).AddToBoard(this->m_player_list.at(index).GetHand().at(playerCard));
			this->m_player_list.at(index).AddToBoard(this->m_table.at(tableCard1)); //adaugarea cartilor in teancul jucatorului
			this->m_player_list.at(index).AddToBoard(this->m_table.at(tableCard2));
			this->m_player_list.at(index).EraseCardPlayer(playerCard);
			EraseCardsTable(tableCard1, tableCard2);

			std::cout << "Do you have another combination? [D/N]\n";
			std::cin >> answer;
		}
		else
		{
			std::cout << "Incorrect combination! Try again.\n";
			std::cout << this->m_player_list.at(index).GetName() << ", which card do you want to use?\n"; std::cin >> playerCard;
			std::cout << this->m_player_list.at(index).GetName() << ", which cards do you want to pull from table?\n"; std::cin >> tableCard1 >> tableCard2;
			TakePoints(index, playerCard, tableCard1, tableCard2);
			std::cout << "Do you have another combination? [D/N]\n";
			std::cin >> answer;
		}
	}

}

void Game::DropCard(int index, int playerCard)
{
	m_table.push_back(this->m_player_list.at(index).GetHand().at(playerCard));
	this->m_player_list.at(index).EraseCardPlayer(playerCard);
}
