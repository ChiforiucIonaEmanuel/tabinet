#pragma once
#include<iostream>

class Card
{
private:
	uint16_t m_symbol;
	uint16_t m_number;


public:
	Card();
	Card(uint16_t symbol, uint16_t number);
	~Card();

	std::string GetSymbol() const;

	std::string GetNumber() const;

	void ShowSymbol();

	void ShowNumber();

	int GetValue();

	friend std::ostream& operator << (std::ostream& os, const Card& card);

	const Card& operator=(const Card& c);

	friend bool operator == (Card card1, Card card2);
};

