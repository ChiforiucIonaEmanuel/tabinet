#pragma once
#include "Player.h"
#include "Card.h"
#include "Deck.h"

class Game
{
private:
	int m_nr_of_players;
	std::vector<Player> m_player_list;
	std::vector<Card> m_table;
	Deck m_deck;


public:
	Game();
	~Game();

	void GameInit();

	void InitPlayers();

	void Run();

	void PrintBanner()const;

	void LoadingInit();

	void SetNrOfPlayers(int nr);

	int GetNrOfPlayers();

	bool NoPlayerCardsLeft();

	void EraseCardsTable(int index1, int index2);

	bool ValidatePlayers()const;

	void ReturnCards();

	Deck GetDeck();

	void Winner();

	int GetIndexFromTable(Card c) const;

	void TakePoints(int index, int playerCard, int tableCard1, int tableCard2);

	void DropCard(int index, int playerCard);
};

