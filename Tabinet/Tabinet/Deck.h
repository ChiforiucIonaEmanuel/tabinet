#pragma once
#include "Card.h"
#include "Player.h"
#include <vector>
class Deck
{
private:
	static const int MAXSIZE = 52;
	
	std::vector<Card>m_deck;

public:
	Deck();
	~Deck();

	void CreateDeck();

	void Shuffle();

	void PrintDeck();

	void PopDeck();

	Card TopDeck();

	void PushBackCard(Card card);

	bool IsFull();

	bool IsEmpty();
	
	Card getCard();

	int GetCardsLeft();
	
};

